# tree-web
> "`tree` is a recursive directory listing command or program that produces a depth-indented listing of files." [wikipedia](https://en.wikipedia.org/wiki/Tree_(command))

`tree-web` is a npm package (`@sctlib/tree-web`) used to create a web page from the output of the `tree` saved as JSON

# Get started
1. Install the dependencies, `tree` and `node` (install with `nvm`)
1. Create a new folder for a new website project `mkdir new-website`
1. Move inside the new directory, `cd new-website`
1. Use the npm command `npx @sctlib/tree-web`
1. Use `npx serve out`, to see the result locally
1. deploy the `out` folder, or use the `.gitlab-ci.yml` file to automate deployment of result as a static page.

# Files
Some files can be used to customize the behavior and output of the program.

> All these files can be used in nested directories, for local behaviors

## .gitignore
Use by git to ignore files, which will also be ignored by tree and tree-web.

## .info
Output information about the files and folder (see .info in this repository file and `man tree`).

## .treeignore
A file to list ignored files and folders, behaves like `.gitignore` (see .tree-web file in this repository).

# Use `tree`
The default `tree` command, can generate an output to render in the command line shell (default behavior), in HTML (`-H`), or in JSON.
```
# output an index.html file from the HTML output of tree
# -I stands for ignore, -T title, -H for the baseHREF & turns on HTML
# -C colorisation, for --prune check the tree man page
tree -C -I '**.css|**.js' --prune -H "https://sctlib.gitlab.io/tree-website/" -T "My tree-website" > index.html

# serve the local site
npx server .
```

This project uses the JSON output, saved together with the HTML and Javascriptcode that will run the website, in the `./out` folder.
