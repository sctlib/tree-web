import { spawn }  from 'node:child_process'

export async function tree() {
		return new Promise((resolve, reject) => {
				const treeCmd = spawn(
						'tree', // unix tree command & parameters...
						[
								'-a', // print all files (event ones starting with dot)
								'-J',  // JSON output
								'--gitignore', // Uses git .gitignore files for filtering
								'--info', // .info files add comments on tree items
								'--gitfile=.treeignore', // use this file like .gitignore
						],
				)
				let result = ''
				treeCmd.stdout.on('data', function(data) {
						result += data.toString()
				});
				treeCmd.on('close', function(code) {
						return resolve(result)
				});
				treeCmd.stderr.on('data', (err) => reject(err.toString()))
		})
}
